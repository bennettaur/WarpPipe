# Overview
WarpPipe is a project for testing out various CI/CD tools. The idea is to 
implement the same (or similar) developer workflow (starting point of a git repo 
ending with deployment to a "production" kubernetes cluser) using some of the 
commonly used CI/CD tools to see how to setup and configure each of them and be 
able to compare them. Think of it as the ToDOMVC equivalent of CI/CD.

As the project goes on I'll slow add more phases and incorporate playing with 
various tools, such as security scanners, to see how all sorts of worflows are 
supported.

The basic CI/CD pipeline will consist of a simple Build-Test-Build-Test-Deploy pipeline 
where each major component of the pipeline will be further divided into 
additional phases (ex. Test -> unit + style + security tests)

Each subdirectory will consist of the files needed to bootstrap each CI/CD 
environment to run in a kubernetes cluster. Potentially you could use these to 
bootstrap your own CI/CD pipeline with some defaults, and just point to your 
source repo, and target deployment environment

## Base Project
One of the ToDo backend projects from [here](https://www.todobackend.com/)

## Workflow & hooks
The workflow will be a basic one consisting of the following high level steps & hooks:
 1. Developer branches off of master to write code
  * This automatically triggers a related ticket to transition from queue/todo to in progress
 2. Pushes branch to repo and opens a PR to master
  * Transitions ticket to Pending review
 3. CI triggers a build & test to run for just the branch
  * Build base images and images for testing
  * Code style tests (ex. linters)
  * Static code security scanning
  * Unit & Integration Tests
 4. On Promotion, build final image and push to repo
 5. Run final image tests
  * Deploy image to test environment and run end to end tests
  * Run performance tests
  * Run additional security tests including image scanning
 6. Deploy to production environment
  * Varies depending on deployment strategy (rolling/canary, blue-green, A/B)

## Basic Setup and Goals
 * Gitlab will be used as a public code and image repo
 * Each CI/CD pipeline will be deployable/deployed and run in a Kubernetes environment
 * Each CI/CD pipeline will send notifications to email & optionally a Slack channel
 * A successful build image will also be deployed to a kubernetes environment

### Goals
 1. Implement the same pipeline using with the following CI/CD tools
  * [Jenkins](https://jenkins.io)
  * [Gitlab CI](https://about.gitlab.com/gitlab-ci/)
  * [Travis CI](https://travis-ci.org)
  * [GoCD](https://gocd.org)
  * [Spinnaker](https://spinnaker.io)
  * [CircleCI](https://circleci.com)
 2. Include at least one of each type of test:
    * Unit Test
    * Integration Test
    * Static Code Analysis (Security)
    * Style Test
    * End to End test
    * Image Security scan
 3. Canaray Deployments
 4. Traceability of images and their builds

### Stretch Goals
 1. Additional CI tools
  * [Codeship](https://codeship.com/)
  * [Shippable](https://www.shippable.com/)
 2. Zero Touch switch between pipelines
 3. Configurable git repo
 4. Configurable image repo
 5. Configurable target environment
    1. Other kubernetes clusters
    2. Cloud environments
 6. Additional notification integrations
 7. Additional sets of tests
  * Performance tests
  * Addtional Security Tests
 8. Examples of additional deployment types
 9. Have visibility into each of the pipeline stages
